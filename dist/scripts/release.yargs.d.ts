import * as Yargs from 'yargs';
export interface Arguments extends Yargs.Arguments {
}
export interface Argv extends Yargs.Argv {
    argv: Arguments;
}
export declare const builder: (yargs?: Yargs.Argv) => Argv;
export declare const handler: (argv: Arguments) => Promise<void>;
