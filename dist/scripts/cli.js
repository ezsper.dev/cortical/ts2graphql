"use strict";
/**
 * This script builds a production output of all of our bundles.
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = require("path");
const cli_yargs_1 = require("./cli.yargs");
const __1 = require("..");
exports.default = (argv) => __awaiter(this, void 0, void 0, function* () {
    const rootPath = path_1.resolve(process.cwd(), argv.project);
    const transpiler = new __1.TypeScript2GraphQL({
        paths: {
            root: rootPath,
            types: argv.typesDir,
            query: argv.queryDir,
            mutation: argv.mutationDir,
            subscription: argv.subscriptionDir,
            validators: argv.validatorsDir,
            scalars: argv.scalarsDir,
        },
    });
    transpiler.dumpSchema(path_1.resolve(process.cwd(), argv.output));
});
function exec() {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            yield cli_yargs_1.handler(cli_yargs_1.builder().argv);
        }
        catch (err) {
            console.log(err.stack !== undefined ? err.stack : err.message);
        }
    });
}
exports.exec = exec;
if (!module.parent) {
    exec();
}
//# sourceMappingURL=cli.js.map