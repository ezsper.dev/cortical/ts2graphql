import * as Yargs from 'yargs';
export interface Arguments extends Yargs.Arguments {
    project: string;
    typesDir: string;
    mutationDir: string;
    queryDir: string;
    subscriptionDir: string;
    validatorsDir: string;
    scalarsDir: string;
    output: string;
}
export interface Argv extends Yargs.Argv {
    argv: Arguments;
}
export declare const builder: (yargs?: Yargs.Argv) => Argv;
export declare const handler: (argv: Arguments) => Promise<void>;
