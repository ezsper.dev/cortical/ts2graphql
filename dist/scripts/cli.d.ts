/**
 * This script builds a production output of all of our bundles.
 */
import { Arguments } from './cli.yargs';
declare const _default: (argv: Arguments) => Promise<void>;
export default _default;
export declare function exec(): Promise<void>;
