"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * This script builds to distribuition
 */
const appRootDir = require("app-root-dir");
const path_1 = require("path");
const fs = require("fs-extra");
const release_yargs_1 = require("./release.yargs");
const exec_1 = require("../utils/exec");
function fileFilter(target, filter, encoding = 'utf8') {
    fs.writeFileSync(target, filter(fs.readFileSync(target).toString(encoding)), encoding);
}
exports.default = (argv) => __awaiter(this, void 0, void 0, function* () {
    // First clear the build output dir.
    exec_1.exec(`rimraf ${path_1.resolve(appRootDir.get(), 'dist')}`);
    // Build typescript
    exec_1.exec('tsc');
    // Copy documents
    fs.copySync(path_1.resolve(appRootDir.get(), 'README.md'), path_1.resolve(appRootDir.get(), 'dist/README.md'));
    // Copy bin
    fs.copySync(path_1.resolve(appRootDir.get(), 'bin'), path_1.resolve(appRootDir.get(), 'dist/bin'));
    // replace ts-node for node
    fileFilter(path_1.resolve(appRootDir.get(), 'dist/bin/ts2graphql'), data => data.replace(/.*ts-node\/register.*/, ''));
    // Clear our package json for release
    const packageJson = require('../package.json');
    packageJson.main = './index.js';
    delete packageJson.scripts;
    delete packageJson['lint-staged'];
    delete packageJson.jest;
    delete packageJson.devDependencies;
    fs.writeFileSync(path_1.resolve(appRootDir.get(), './dist/package.json'), JSON.stringify(packageJson, null, 2));
    // Copy .gitignore
    exec_1.exec('cp .gitignore dist/');
});
if (!module.parent) {
    release_yargs_1.handler(release_yargs_1.builder().argv)
        .catch(err => {
        console.log(err.stack);
    });
}
//# sourceMappingURL=release.js.map