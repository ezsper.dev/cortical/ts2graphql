"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Yargs = require("yargs");
exports.builder = (yargs = Yargs) => yargs
    .option('output', {
    alias: 'o',
    describe: 'Output path to store generated .graphql files',
    type: 'string',
    default: 'schema'
})
    .option('project', {
    alias: 'p',
    describe: 'The project path to look for directories',
    type: 'string',
    default: '.',
})
    .option('types-dir', {
    alias: 't',
    describe: 'The types dir where single types are located',
    type: 'string',
    default: './types',
})
    .option('mutation-dir', {
    alias: 'm',
    describe: 'The mutation dir where single methods are located',
    type: 'string',
    default: './mutation',
})
    .option('query-dir', {
    alias: 'q',
    describe: 'The query dir where single methods are located',
    type: 'string',
    default: './query',
})
    .option('subscription-dir', {
    alias: 's',
    describe: 'The subscription dir where single methods are located',
    type: 'string',
    default: './subscription',
})
    .option('validators-dir', {
    alias: 's',
    describe: 'The dir where type validators stays',
    type: 'string',
    default: './validators',
})
    .option('scalars-dir', {
    alias: 's',
    describe: 'The dir where graphql scalars are located',
    type: 'string',
    default: './scalars',
});
exports.handler = (argv) => __awaiter(this, void 0, void 0, function* () {
    yield (yield Promise.resolve().then(() => require('./cli'))).default(argv);
});
//# sourceMappingURL=cli.yargs.js.map