import { Handler as CreateTypeDocJsonHandler } from './utils/createTypeDocJson';
import { TypeDoc2GraphQLOptions, TypeDoc2GraphQLPaths, TypeDoc2GraphQL, Parser } from './utils/TypeDoc2GraphQL';
import { IResolvers, IResolverValidationOptions } from 'graphql-tools/dist/Interfaces';
import { GraphQLObjectType, GraphQLNamedType, GraphQLSchema, GraphQLResolveInfo } from 'graphql';
import { Validator } from './utils/Validator';
export * from './utils/TypeDoc2GraphQL';
export * from './utils/Validator';
export interface SourceMapField {
    sanitize?: Parser[];
    validate?: Parser[];
    sanitizeArgs?: {
        [arg: string]: Parser[];
    };
    validateArgs?: {
        [arg: string]: Parser[];
    };
}
export interface SourceMap {
    name: string;
    type: string;
    fields?: {
        [field: string]: SourceMapField;
    };
    values?: {
        [key: string]: number | string;
    };
}
export interface TypeScript2GraphQLPaths extends TypeDoc2GraphQLPaths {
    root: string;
    validators: string;
    scalars: string;
}
export interface TypeScript2GraphQLOptions extends TypeDoc2GraphQLOptions {
    paths?: Partial<TypeDoc2GraphQLPaths> & Partial<TypeScript2GraphQLPaths>;
}
export declare type Resolver<T, A extends {} | void = void, C extends any = any> = (args: A, context: C, info?: GraphQLResolveInfo) => T | Promise<T>;
export declare type AsyncIteratorSubscriber<T, A extends {} | void = void, C extends any = any> = (args: A, context: C, info?: GraphQLResolveInfo) => AsyncIterator<T> | AsyncIterableIterator<T>;
export declare namespace Observable {
    interface Disposable {
        dispose(): void;
    }
    interface Observable<T> {
        subscribe(onNext?: (value: T) => void, onError?: (exception: any) => void, onCompleted?: () => void): Disposable;
    }
}
export declare type ObservableSubscriber<T, A extends {} | void = void, C extends any = any> = (args: A, context: C, info?: GraphQLResolveInfo) => Observable.Observable<T>;
export declare type Subscriber<T, A extends {} | void = void, C extends any = any> = AsyncIteratorSubscriber<T, A, C> | ObservableSubscriber<T, A, C>;
export declare type SubscriptionResolver<T, A extends {} | void = void, C extends any = any> = Resolver<T, A, C>;
export declare function graphqlParseOutput(value: any, context: any, info: any): Promise<any>;
export declare function getValidator(rootPath: string, validatorDir: string): Validator;
export declare function getScalars(rootPath: string, scalarsDir: string): GraphQLNamedType[];
export declare class TypeScript2GraphQL extends TypeDoc2GraphQL {
    protected rootPath: string;
    protected paths: TypeScript2GraphQLPaths;
    constructor(options?: TypeScript2GraphQLOptions);
    generate(handler?: CreateTypeDocJsonHandler, stdio?: any): void;
    restoreSourceMap(type: GraphQLNamedType, map: SourceMap): void;
    restoreSchema(inputDir?: string): GraphQLSchema;
    dumpSchema(outputDir?: string): void;
    getSourceMap(type: any): void | SourceMap;
    injectParserHandlerOnObjectType(type: GraphQLObjectType): void;
    injectParserHandler(schema: GraphQLSchema): void;
    addResolveFunctionsToSchema(schema: GraphQLSchema, resolvers: IResolvers, resolverValidationOptions?: IResolverValidationOptions): void;
    getSource(type: any): string;
    getMethodResolvers(dir: string, name: string): IResolvers;
    getTypeResolvers(schema: GraphQLSchema): IResolvers;
    getResolvers(schema: GraphQLSchema): IResolvers;
    getSchemaConfig(handler?: CreateTypeDocJsonHandler, stdio?: any): any;
    getSchema(): GraphQLSchema;
    getSchemaWithResolvers(): GraphQLSchema;
    restoreSchemaWithResolvers(inputDir?: string): GraphQLSchema;
}
