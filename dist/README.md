# Cortical GraphQL Transpiler

A runtime tranpiler and generator that converts your TypeScript code into GraphQL types

## Key aspects

* It doesn't use decorators to transpile code
* It inflicts a folder/file export pattern
* Depends on TypeDoc JSON generator

## How to use it

Set your enviroment to have the following directories

### ./types

Include all GraphQL types in separated files that export the TypeScript type.

#### Interface

Interface TypeScript types are converted to GraphQL Input Object Type

##### input
```
// types/Login.ts
/**
 * Login input
 */
export interface Login {
  /**
   * The user's email address
   */
  email: string;
  /**
   * The user's plain password
   */
  password: string;
}
```

##### output

```
// schema/Login.graphql
# Login input
input Login {
  # The user's email address
  email: String!
  # The user's plain password
  password: String!
}
```

#### Class

Class TypeScript types are converted to GraphQL Object Type

```
// types/User.ts
import { Post } from './Post';

/**
 * User object
 */
export class User {
  /**
   * The user's unique id
   */
  id: string;
  /**
   * The user's email address
   */
  email: string;
  /**
   * The user's plain password
   * @GraphQL.disable
   */
  password: string;
  /**
   * Get latest user's posts
   */
  async getLatestPosts(
    args: {
      /**
       * The maximum number of posts to fetch
       */
      limit?: number = 10
    },
    context: any
  ): Promise<Post[]> {
    // ...
  }
  /**
   * Check user password
   * @GraphQL.disable
   */
  checkPassword(password: string): boolean {
    // ...
  }
}
```
##### output

```
// schema/User.graphql
# User object
type User {
  # The user's unique id
  id: String!
  # The user's email address
  email: String!
  # Get latest user's posts
  getLatestPosts(
    # The maximum number of posts to fetch
    limit: Float = 10
  ): [Post]
}
```

## It comes with a CLI

```
ts2graphql -p src -o build/graphql
```

> TODO: More examples and tutorials
