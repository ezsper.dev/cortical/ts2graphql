import { ParserHandler, ParserHandlerParseOptions } from './TypeDoc2GraphQL';
import { GraphQLType, GraphQLResolveInfo } from 'graphql';
export declare type ValidatorParseHandler<T, R> = (value: T, source: any, context: any, info: GraphQLResolveInfo) => R | Promise<R>;
export declare type ValidatorHandler<T, R> = (...args: any[]) => ValidatorParseHandler<T, R>;
export declare type ValidatorSanitizeMap<T> = {
    [methodName: string]: ValidatorHandler<T, T>;
};
export declare type ValidatorValidateMap<T> = {
    [methodName: string]: ValidatorHandler<T, void>;
};
export interface ValidatorMap<T> {
    sanitize: ValidatorSanitizeMap<T>;
    validate: ValidatorValidateMap<T>;
}
export interface ValidatorTypeMap {
    [typeName: string]: ValidatorMap<any>;
}
export declare class Validator implements ParserHandler {
    private map;
    constructor(map?: ValidatorTypeMap);
    getTypeSanitizer<T>(typeName: string, name: string): ValidatorHandler<T, T> | void;
    getTypeValidator<T>(typeName: string, name: string): ValidatorHandler<T, void> | void;
    getTypeAction<T>(typeName: string, action: 'sanitize' | 'validate', name: string): void | ValidatorHandler<T, T> | ValidatorHandler<{}, void>;
    test(action: 'sanitize' | 'validate', valueType: GraphQLType, name: string, args: (boolean | string | number | null)[]): any;
    parse(options: ParserHandlerParseOptions): Promise<void>;
}
