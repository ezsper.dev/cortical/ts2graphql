"use strict";
/*
 * This is a TypeDoc traversing that help us get informations about the
 * project files
 */
Object.defineProperty(exports, "__esModule", { value: true });
class TypeDoc {
    constructor(ref) {
        this.index = {};
        this.parent = {};
        this.externalModules = {};
        this.dirs = {};
        const parseDoc = (ref) => {
            this.index[ref.id] = ref;
            if (ref.name === 'Resolver') {
                console.log('**', ref);
            }
            const parseChild = (child) => {
                this.parent[child.id] = ref;
                parseDoc(child);
            };
            if (ref.children !== undefined) {
                ref.children.forEach(parseChild);
            }
            if (ref.signatures !== undefined) {
                ref.signatures.forEach(parseChild);
            }
            if (ref.parameters !== undefined) {
                ref.parameters.forEach(parseChild);
            }
            if (typeof ref.type !== 'undefined') {
                if (ref.getSignature) {
                    parseChild(ref.getSignature);
                }
                if (ref.setSignature) {
                    parseChild(ref.setSignature);
                }
                let types = [ref.type];
                if (ref.type.types !== undefined) {
                    types = types.concat(ref.type.types);
                }
                for (const type of types) {
                    if (type.declaration) {
                        parseChild(type.declaration);
                    }
                }
            }
            if (this.isExternalModule(ref) && ref.originalName !== undefined) {
                this.externalModules[ref.originalName] = ref;
                const dir = ref.originalName.replace(/\/([^\/]+)$/, '');
                if (this.dirs[dir] === undefined) {
                    this.dirs[dir] = [];
                }
                this.dirs[dir].push(ref);
            }
            return Object.freeze(ref);
        };
        this.root = parseDoc(ref);
    }
    getRoot() {
        return this.root;
    }
    isRoot(ref) {
        return ref === this.root;
    }
    hasParent(id) {
        return this.parent.hasOwnProperty(`${id}`);
    }
    getParent(id) {
        const ref = this.parent[id];
        if (ref === undefined) {
            throw new Error('Could not find parent');
        }
        return ref;
    }
    getParentExternalModule(id) {
        const ref = this.get(id);
        if (this.isExternalModule(ref)) {
            return ref;
        }
        if (this.hasParent(id)) {
            return this.getParentExternalModule(this.getParent(id).id);
        }
        throw new Error('Could not get an external parent');
    }
    has(id) {
        return this.index.hasOwnProperty(`${id}`);
    }
    get(id) {
        const ref = this.index[id];
        if (ref === undefined) {
            throw new Error('Could not find index');
        }
        return ref;
    }
    hasExternalModule(fileName) {
        return this.externalModules.hasOwnProperty(fileName);
    }
    // TODO: rename to findExternalModule
    getExternalModule(fileName) {
        const ref = this.externalModules[fileName];
        if (ref === undefined) {
            throw new Error('Could not find external module');
        }
        return ref;
    }
    isExternalModule(ref) {
        return (ref.kindString === 'External module'
            && ref.flags.isExported
            && Array.isArray(ref.sources)
            && ref.sources.length >= 1);
    }
    hasPath(path) {
        return this.dirs.hasOwnProperty(path);
    }
    getPath(path) {
        const externalModules = this.dirs[path];
        if (externalModules === undefined) {
            throw new Error('Could not find path');
        }
        return externalModules.slice();
    }
}
exports.TypeDoc = TypeDoc;
//# sourceMappingURL=TypeDoc.js.map