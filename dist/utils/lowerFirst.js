"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function lowerFirst(value) {
    return value.charAt(0).toLowerCase() + value.slice(1);
}
exports.lowerFirst = lowerFirst;
//# sourceMappingURL=lowerFirst.js.map