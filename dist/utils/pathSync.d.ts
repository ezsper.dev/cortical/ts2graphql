export declare function pathSync(targetDir: string, { isRelativeToScript }?: {
    isRelativeToScript?: boolean | undefined;
}): void;
