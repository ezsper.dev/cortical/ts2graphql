"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function values(obj) {
    if (typeof Object.values !== 'undefined') {
        return Object.values(obj);
    }
    const values = [];
    for (const key of Object.keys(obj)) {
        values.push(obj[key]);
    }
    return values;
}
exports.values = values;
//# sourceMappingURL=values.js.map