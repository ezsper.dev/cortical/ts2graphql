"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const child_process_1 = require("child_process");
const appRootDir = require("app-root-dir");
const TypeDoc_1 = require("./TypeDoc");
const path = require("path");
const fs = require("fs");
function createTypeDocJson(inputPath, handler, stdio = 'inherit') {
    const tmpDir = path.resolve(process.cwd(), '.tmp');
    const astJsonPath = path.resolve(tmpDir, `typedoc.json`);
    const result = child_process_1.spawnSync('typedoc', [
        '--excludeExternals',
        '--json', astJsonPath,
        inputPath,
    ], {
        stdio,
        env: {
            PATH: process.env.PATH,
        },
        cwd: appRootDir.get(),
    });
    if (typeof handler === 'function') {
        handler(result);
    }
    if (result.signal != null) {
        throw new Error(`Ended process due to signal ${result.signal}`);
    }
    if (result.error != null) {
        throw result.error;
    }
    if (result.status !== 0) {
        throw new Error(`Could not generate due to typescript error`);
    }
    const typedocJson = require(astJsonPath);
    fs.unlinkSync(astJsonPath);
    try {
        fs.rmdirSync(tmpDir);
    }
    catch (err) {
    }
    return new TypeDoc_1.TypeDoc(typedocJson);
}
exports.createTypeDocJson = createTypeDocJson;
//# sourceMappingURL=createTypeDocJson.js.map