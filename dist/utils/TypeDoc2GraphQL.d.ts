import { TypeDoc, TypeDocRef } from './TypeDoc';
import { GraphQLObjectType, GraphQLSchema, GraphQLNamedType, GraphQLResolveInfo, GraphQLType } from 'graphql';
export interface Parser {
    name: string;
    args: (boolean | string | number | null)[];
}
export interface ParserRule extends Parser {
    path: string[];
    valueType: GraphQLType;
}
export interface ParserHandlerParseOptions {
    type: GraphQLObjectType;
    fieldName: string;
    args: {
        [argName: string]: any;
    };
    sanitizeRules: ParserRule[];
    validateRules: ParserRule[];
    source: any;
    context: any;
    info: GraphQLResolveInfo;
}
export declare type ParserHandlerTest = (action: 'sanitize' | 'validate', valueType: GraphQLNamedType, name: string, args: (boolean | string | number | null)[]) => Error | void;
export declare type ParserHandlerParse = (options: ParserHandlerParseOptions) => void | Promise<void>;
export interface ParserHandler {
    test: ParserHandlerTest;
    parse: ParserHandlerParse;
}
export interface TypeDoc2GraphQLPaths {
    types: string;
    query: string;
    mutation: string;
    subscription: string;
}
export interface TypeDoc2GraphQLOptions {
    types?: GraphQLNamedType[];
    parserHandler?: ParserHandler;
    paths?: Partial<TypeDoc2GraphQLPaths>;
}
export declare class TypeDoc2GraphQL {
    protected typedoc: TypeDoc;
    protected options: TypeDoc2GraphQLOptions;
    protected resolvedTypes: {
        [key: string]: GraphQLNamedType;
    };
    protected types: {
        [name: string]: GraphQLNamedType;
    };
    protected paths: TypeDoc2GraphQLPaths;
    protected query: GraphQLObjectType | void;
    protected mutation: GraphQLObjectType | void;
    protected subscription: GraphQLObjectType | void;
    protected parserHandler: ParserHandler;
    constructor(options: TypeDoc2GraphQLOptions);
    initialize(doc: TypeDocRef | TypeDoc): void;
    getTypeDoc(): TypeDoc;
    private relativeError;
    private getFileExport;
    private resolveIntrinsicType;
    private resolveExternalUnionType;
    private resolveExternalType;
    private resolveInterfaceMethodArg;
    private resolveInterfaceMethodArgs;
    private parseValueArg;
    parseValueJSON(tagText: string): any;
    private parseValue;
    private resolveInterfacePropertyAsField;
    private resolveInterfaceType;
    resolveFields(ref: TypeDocRef, isInput?: boolean): any;
    private resolveClassType;
    private resolveEnumerationType;
    private resolveType;
    private resolveMethod;
    getCustomTypes(): any;
    hasCustomType(name: string): boolean;
    getCustomType(name: string): GraphQLNamedType;
    getTypes(): GraphQLNamedType[];
    hasType(name: string): boolean;
    getType(name: string): GraphQLNamedType;
    private getExportResolveFields;
    private getResolveFields;
    private getRootType;
    getQueryType(): GraphQLObjectType;
    getMutationType(): void | GraphQLObjectType;
    getSubscriptionType(): void | GraphQLObjectType;
    getSchemaConfig(): any;
    getSchema(): GraphQLSchema;
}
