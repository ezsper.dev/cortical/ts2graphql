"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const apollo_server_errors_1 = require("apollo-server-errors");
const graphql_1 = require("graphql");
function findNamedType(type) {
    if ('ofType' in type) {
        return findNamedType(type.ofType);
    }
    return type;
}
function getPath(obj, path) {
    let result = obj;
    for (const part of path) {
        result = result[part];
        if (result === undefined) {
            break;
        }
    }
    return result;
}
function setPath(obj, path, value) {
    let result = obj;
    const parts = path.slice(0, path.length - 1);
    const name = path[path.length - 1];
    for (const part of parts) {
        result = result[part];
        if (result === undefined) {
            return;
        }
    }
    result[name] = value;
}
class Validator {
    constructor(map = {}) {
        this.map = map;
    }
    getTypeSanitizer(typeName, name) {
        if (typeName in this.map && name in this.map[typeName].sanitize) {
            return this.map[typeName].sanitize[name];
        }
    }
    getTypeValidator(typeName, name) {
        if (typeName in this.map && name in this.map[typeName].validate) {
            return this.map[typeName].validate[name];
        }
    }
    getTypeAction(typeName, action, name) {
        if (action === 'sanitize') {
            return this.getTypeSanitizer(typeName, name);
        }
        return this.getTypeValidator(typeName, name);
    }
    test(action, valueType, name, args) {
        const isNonNull = valueType instanceof graphql_1.GraphQLNonNull;
        const isList = (valueType instanceof graphql_1.GraphQLNonNull && 'ofType' in valueType)
            ? valueType.ofType instanceof graphql_1.GraphQLList
            : valueType instanceof graphql_1.GraphQLList;
        const type = findNamedType(valueType);
        const actionHandler = this.getTypeAction(type.name, action, name);
        if (!actionHandler) {
            throw new Error(`Unknown ${action}.${name} method for ${type.name}`);
        }
        try {
            actionHandler(...args);
        }
        catch (error) {
            return error;
        }
    }
    parse(options) {
        return __awaiter(this, void 0, void 0, function* () {
            const { args, source, context, info, fieldName, sanitizeRules, validateRules, } = options;
            for (const { valueType, path, name, args: methodArgs } of sanitizeRules) {
                try {
                    const isNonNull = valueType instanceof graphql_1.GraphQLNonNull;
                    const isList = (valueType instanceof graphql_1.GraphQLNonNull && 'ofType' in valueType)
                        ? valueType.ofType instanceof graphql_1.GraphQLList
                        : valueType instanceof graphql_1.GraphQLList;
                    const type = findNamedType(valueType);
                    const value = getPath(args, path);
                    const method = this.getTypeSanitizer(type.name, name);
                    if (value === undefined || !method) {
                        continue;
                    }
                    let parsedValue;
                    if (Array.isArray(value) && isList) {
                        parsedValue = [];
                        for (const item of value) {
                            parsedValue.push(yield (method(...methodArgs)(item, source, context, info)));
                        }
                    }
                    else {
                        parsedValue = yield (method(...methodArgs)(value, source, context, info));
                    }
                    setPath(args, path, parsedValue);
                }
                catch (error) {
                    if (error instanceof graphql_1.GraphQLError) {
                        let errorPath = [];
                        if (info.path !== undefined) {
                            let { prev } = info.path;
                            while (prev !== undefined) {
                                errorPath.push(prev.key);
                                prev = prev.prev;
                            }
                            errorPath.push(info.path.key);
                        }
                        errorPath = errorPath.concat(['$args'], path.slice(1));
                        throw new graphql_1.GraphQLError(error.message, error.nodes !== undefined ? error.nodes : info.fieldNodes, error.source !== undefined ? error.source : source, error.positions, errorPath, error.originalError);
                    }
                    throw error;
                }
            }
            for (const { valueType, path, name, args: methodArgs } of validateRules) {
                try {
                    const isNonNull = valueType instanceof graphql_1.GraphQLNonNull;
                    const isList = (valueType instanceof graphql_1.GraphQLNonNull && 'ofType' in valueType)
                        ? valueType.ofType instanceof graphql_1.GraphQLList
                        : valueType instanceof graphql_1.GraphQLList;
                    const type = findNamedType(valueType);
                    const value = getPath(args, path);
                    const method = this.getTypeValidator(type.name, name);
                    if (value === undefined || !method) {
                        continue;
                    }
                    if (Array.isArray(value) && isList) {
                        for (const item of value) {
                            yield (method(...methodArgs)(item, source, context, info));
                        }
                    }
                    else {
                        yield (method(...methodArgs)(value, source, context, info));
                    }
                }
                catch (error) {
                    if ((error instanceof graphql_1.GraphQLError || error instanceof apollo_server_errors_1.ApolloError)) {
                        let errorPath = [];
                        if (info.path !== undefined) {
                            let { prev } = info.path;
                            while (prev !== undefined) {
                                errorPath.push(prev.key);
                                prev = prev.prev;
                            }
                            errorPath.push(info.path.key);
                        }
                        errorPath = errorPath.concat(['$args'], path);
                        if (error.path !== undefined) {
                            errorPath = errorPath.concat(Array.isArray(error.path) ? error.path : [error.path]);
                        }
                        throw apollo_server_errors_1.fromGraphQLError(new graphql_1.GraphQLError(error.message, error.nodes !== undefined ? error.nodes : info.fieldNodes, error.source !== undefined ? error.source : source, error.positions, errorPath, error.originalError), {
                            errorClass: (error instanceof apollo_server_errors_1.ApolloError ? error.constructor : apollo_server_errors_1.ValidationError),
                        });
                    }
                    throw error;
                }
            }
        });
    }
}
exports.Validator = Validator;
//# sourceMappingURL=Validator.js.map