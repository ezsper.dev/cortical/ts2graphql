/// <reference types="node" />
import { SpawnSyncReturns } from 'child_process';
import { TypeDoc } from './TypeDoc';
export declare type Handler = (result: SpawnSyncReturns<Buffer>) => void;
export declare function createTypeDocJson(inputPath: string, handler?: Handler, stdio?: any): TypeDoc;
