export declare type TypeDocRefType = {
    type: string;
    types?: TypeDocRefType[];
    name: string;
    id?: number;
    typeArguments?: TypeDocRefType[];
    elementType?: TypeDocRefType;
    declaration?: TypeDocRef;
};
export declare type TypeDocRefFlags = {
    isExported?: boolean;
    isPrivate?: boolean;
    isStatic?: boolean;
    isProtected?: boolean;
    isOptional?: boolean;
};
export declare type TypeDocRefSource = {
    fileName: string;
    line: number;
    character: number;
};
export declare type TypeDocRef = {
    id: number;
    name: string;
    kind: number;
    kindString?: string;
    children?: TypeDocRef[];
    signatures?: TypeDocRef[];
    getSignature?: TypeDocRef;
    setSignature?: TypeDocRef;
    parameters?: TypeDocRef[];
    parent?: TypeDocRef;
    flags: TypeDocRefFlags;
    originalName?: string;
    comment?: {
        shortText?: string;
        tags: {
            tag: string;
            text: string;
        }[];
    };
    sources?: TypeDocRefSource[];
    defaultValue?: any;
    type?: TypeDocRefType;
    implementationOf?: TypeDocRefType;
    overwrites?: TypeDocRefType;
    implementedTypes?: TypeDocRefType[];
    extendedTypes?: TypeDocRefType[];
    groups: TypeDocRef[];
};
export declare class TypeDoc {
    private root;
    private index;
    private parent;
    private externalModules;
    private dirs;
    constructor(ref: TypeDocRef);
    getRoot(): TypeDocRef;
    isRoot(ref: TypeDocRef): boolean;
    hasParent(id: number): boolean;
    getParent(id: number): TypeDocRef;
    getParentExternalModule(id: number): TypeDocRef;
    has(id: number): boolean;
    get(id: number): TypeDocRef;
    hasExternalModule(fileName: string): boolean;
    getExternalModule(fileName: string): TypeDocRef;
    isExternalModule(ref: TypeDocRef): boolean | undefined;
    hasPath(path: string): boolean;
    getPath(path: string): TypeDocRef[];
}
