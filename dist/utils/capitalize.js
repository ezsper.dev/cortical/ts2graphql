"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function capitalize(value) {
    return value.charAt(0).toUpperCase() + value.slice(1);
}
exports.capitalize = capitalize;
//# sourceMappingURL=capitalize.js.map