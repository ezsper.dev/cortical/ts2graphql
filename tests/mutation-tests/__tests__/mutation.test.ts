declare var jest, describe, it, expect;
import "@spine/polyfill";
import { resolve as pathResolve } from 'path';
import { isAsyncIterable } from 'iterall';
import * as typeMap from '@cortical/types/graphql';

import { TypeScript2GraphQL } from '../../..';
import gql from 'graphql-tag';
import {
  GraphQLObjectType,
  GraphQLUnionType,
  GraphQLInputObjectType,
  GraphQLEnumType,
  graphql,
  subscribe,
  parse, GraphQLInterfaceType
} from 'graphql';
const types: any[] = [];
for (const key in typeMap) {
  types.push((<any>typeMap)[key]);
}
describe('Types', () => {
  const options = {
    types,
    paths: {
      root: pathResolve(__dirname, '..', 'src'),
    },
  };
  const transpiler = new TypeScript2GraphQL(options);
  const schema = transpiler.getSchemaWithResolvers();
  it('User should be GraphQLObjectType', () => {
    const User = schema.getType('User');
    expect(User).toBeDefined();
    expect(User).toBeInstanceOf(GraphQLObjectType);
    const source = transpiler.getSource(User);
    expect(source).toBe(
`"""The User"""
type User implements Actor & Node {
  """The user display name"""
  displayName: String!

  """The user email"""
  email: Email

  """The user family / last name"""
  familyName: String!
  friends: [User!]!

  """The user gender"""
  gender: Gender!

  """The user given / first name"""
  givenName: String!

  """The user\\'s node id"""
  id: ID!

  """The user website"""
  website: String
}`
    );
  });

  it('UserRegistration should be GraphQLInputObjectType', () => {
    const RegisterUserInput = schema.getType('RegisterUserInput');
    expect(RegisterUserInput).toBeDefined();
    expect(RegisterUserInput).toBeInstanceOf(GraphQLInputObjectType);
    const source = transpiler.getSource(RegisterUserInput);
    expect(source).toBe(
`"""The User Registration Input"""
input RegisterUserInput {
  clientMutationId: String

  """The user email"""
  email: Email!

  """The user family / last name"""
  familyName: String!

  """The user gender"""
  gender: Gender = OTHER

  """The user given / first name"""
  givenName: String!

  """The user website"""
  website: String
}`
    );
  });

  it('Gender should be GraphQLEnumType', () => {
    const Gender = schema.getType('Gender');
    expect(Gender).toBeDefined();
    expect(Gender).toBeInstanceOf(GraphQLEnumType);
    const source = transpiler.getSource(Gender);
    expect(source).toBe(
`enum Gender {
  FEMALE
  MALE
  OTHER
}`
    );
  });

  it('Mutation should be GraphQLObjectType', () => {
    const Mutation = schema.getMutationType();
    expect(Mutation).toBeDefined();
    expect(Mutation).toBeInstanceOf(GraphQLObjectType);
    const source = transpiler.getSource(Mutation);
    expect(source).toBe(
`type Mutation {
  inviteUser(input: InviteUserInput!): InviteUserResult!
  registerUser(
    """The User Registration Input"""
    input: RegisterUserInput!
  ): RegisterUserResult!
}`
    );
  });

  it('RegisterUserResult should be GraphQLObjectType', () => {
    const RegisterUserResult = schema.getType('RegisterUserResult');
    expect(RegisterUserResult).toBeDefined();
    expect(RegisterUserResult).toBeInstanceOf(GraphQLObjectType);
    const source = transpiler.getSource(RegisterUserResult);
    expect(source).toBe(
`type RegisterUserResult {
  clientMutationId: String
  user: User!
}`
    );
  });

  it('InviteUserInput should be GraphQLObjectType', () => {
    const InviteUserInput = schema.getType('InviteUserInput');
    expect(InviteUserInput).toBeDefined();
    expect(InviteUserInput).toBeInstanceOf(GraphQLInputObjectType);
    const source = transpiler.getSource(InviteUserInput);
    expect(source).toBe(
      `input InviteUserInput {
  clientMutationId: String
  entries: [InviteUserEntryInput!]!
}`
    );
  });

  it('InstagramMedia should be GraphQLObjectType', () => {
    const InstagramMedia = schema.getType('InstagramMedia');
    expect(InstagramMedia).toBeDefined();
    expect(InstagramMedia).toBeInstanceOf(GraphQLObjectType);
    const source = transpiler.getSource(InstagramMedia);
    expect(source).toBe(
      `type InstagramMedia implements Media {
  children: [InstagramMedia!]!
  childrenIds: [ID!]!
  host: FQDN!
  id: ID!
  instagramShortcode: String!
  isVerified: Boolean!
  uri: URI!
}`
    );
  });

  it('Media should be GraphQLInterfaceType', () => {
    const Media = schema.getType('Media');
    expect(Media).toBeDefined();
    expect(Media).toBeInstanceOf(GraphQLInterfaceType);
    const source = transpiler.getSource(Media);
    expect(source).toBe(
      `interface Media {
  children: [Media!]!
  childrenIds: [ID!]!
  isVerified: Boolean!
  uri: URI!
  id: ID!
  host: FQDN!
}`
    );
  });

  it('SearchResult should be GraphQLUnionType', () => {
    const SearchResult = schema.getType('SearchResult');
    expect(SearchResult).toBeDefined();
    expect(SearchResult).toBeInstanceOf(GraphQLUnionType);
    const source = transpiler.getSource(SearchResult);
    expect(source).toBe(
      'union SearchResult = User | Organization',
    );
  });

  it('Query should be GraphQLObjectType', () => {
    const Query = schema.getQueryType();
    expect(Query).toBeDefined();
    expect(Query).toBeInstanceOf(GraphQLObjectType);
    const source = transpiler.getSource(Query);
    expect(source).toBe(
      `type Query {
  foo: String!
  node(id: String!): Node!
  search(after: String, before: String, first: Int, last: Int): SearchResultConnection!
  user(id: ID!): User!
}`,
    );
  });

  it('SearchResultConnection should be GraphQLObjectType', () => {
    const SearchResultConnection = schema.getType('SearchResultConnection');
    expect(SearchResultConnection).toBeDefined();
    expect(SearchResultConnection).toBeInstanceOf(GraphQLObjectType);
    const source = transpiler.getSource(SearchResultConnection);
    expect(source).toBe(
`"""The standardized Relay object for dealing with pagination on Graph"""
type SearchResultConnection {
  """The connection edges"""
  edges: [SearchResultConnectionEdge]

  """The connection nodes"""
  nodes: [SearchResult]

  """The connection page info"""
  pageInfo: ConnectionPageInfo!
}`
    );
  });

  it('RegisterUserInput source map should have validation', () => {
    const RegisterUserInput = schema.getType('RegisterUserInput');
    expect(RegisterUserInput).toBeDefined();
    const map = <any>transpiler.getSourceMap(RegisterUserInput);
    expect(map).toBeDefined();
    expect(map.fields).toBeDefined();
    expect(map.fields.givenName).toBeDefined();
    expect(map.fields.givenName.validate).toBeDefined();
    expect(map.fields.givenName.validate).toEqual([
      { name: 'min', args: [3] },
      { name: 'max', args: [50] },
    ]);
    expect(map.fields.givenName.sanitize).toEqual([{ name: 'text', args: [] }]);
  });

  it('Execute registerUser', async () => {
    const query = `mutation {
      payload: registerUser(
        input: {
          clientMutationId: "My mutation id"
          givenName: "        My   name "
          familyName: "   My family name"
          email: "john@example.com"
          website: "www.mysite.com"
        }
      ) {
        clientMutationId
        user {
          givenName
          gender
          website
        }
      }
    }`;
    const result = <any>(await graphql(schema, query, {}));
    expect(result.data).toBeDefined();
    expect(result.data.payload).toBeDefined();
    expect(result.data.payload.user).toBeDefined();
    expect(result.data.payload.user.givenName).toEqual('My name');
    expect(result.data.payload.user.gender).toEqual('OTHER');
    expect(result.data.payload.user.website).toEqual('http://www.mysite.com');
  });


  it('Execute subscription.message', async () => {
    const query = `subscription {
      message: message(
        channel: "test"
      )
    }`;
    const iterable = await subscribe(schema, parse(query), {});
    expect(isAsyncIterable(iterable)).toBe(true);
    const messages = [
      'Hi!',
      'What is your name?',
      'Nice to meet you!',
    ];
    for await (const result of iterable) {
      expect(result).toEqual({ data: { message: messages.shift() } });
    }
  });

  it('Execute subscription.notification', async () => {
    const query = `subscription {
      notification
    }`;
    const iterable = await subscribe(schema, parse(query), {});
    expect(isAsyncIterable(iterable)).toBe(true);
    const notifications = [
      'You have 1 more message',
      'You have 2 more messages',
      'You have 3 more messages',
    ];
    for await (const result of iterable) {
      expect(result).toEqual({ data: { notification: notifications.shift() } });
    }
  });
});
