import { Subscriber } from '../../../..';

export interface Arguments {
  channel: string;
}

const messages = [
  'Hi!',
  'What is your name?',
  'Nice to meet you!',
];

export const subscribeMessage: Subscriber<string, Arguments> = async function *(args) {
  for (const message of messages) {
    yield message;
  }
};
