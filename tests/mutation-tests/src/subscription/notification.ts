import { Observable } from 'rx';
import { Subscriber } from '../../../..';

const notifications = [
  'You have 1 more message',
  'You have 2 more messages',
  'You have 3 more messages',
];

export const subscribe: Subscriber<string, {}> = (args, context) => {
  return Observable.from(notifications);
}
