import {
  GraphQLScalarType,
  GraphQLError,
  Kind,
} from 'graphql';

function isEmail(value: string) {
  return value.indexOf('@') >= 0;
}

export const GraphQLEmail = new GraphQLScalarType({
  name: 'Email',
  description: `Email string type`,
  serialize(value) {
    if (typeof value !== 'string') {
      return null
    }
    if (isEmail(value)) {
      return value
    }
    return null
  },
  parseValue(value) {
    if (typeof value !== 'string') {
      throw new GraphQLError('', [])
    }
    if (isEmail(value)) {
      return value
    }
    throw new GraphQLError('', [])
  },
  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new GraphQLError(`Type should be "String", found ${ast.kind}.`, [ast])
    }
    if (isEmail(ast.value)) {
      return ast.value;
    }
    throw new GraphQLError(`Invalid Email literal.\n${ast.value} is not Email.`, [ast])
  }
});
