import { ID, GlobalId } from '@cortical/types';
import { User } from '../types/User';
import { Gender } from '../types/Gender';

export async function resolveUser(args: { id: ID }): Promise<User> {
  const { id } = args;
  const databaseId = GlobalId.parse(id, 'String');
  return new User({
    databaseId,
    displayName: 'John Doe',
    givenName: 'John',
    familyName: 'Doe',
    gender: Gender.MALE,
    email: 'john@example.com',
    website: 'johndoe.com',
  });
}
