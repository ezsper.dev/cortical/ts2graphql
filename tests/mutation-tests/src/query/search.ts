import { User } from '../types/User';
import { Organization } from '../types/Organization';
import { Gender } from '../types/Gender';
import {
  Connection,
  ConnectionArguments,
  connectionFromArray,
} from '@cortical/types';

export interface Arguments extends ConnectionArguments {}

export type Result = User | Organization;

export function resolveSearch(args: Arguments): Connection<Result> {
  const nodes = [
    new User({
      databaseId: '1',
      displayName: 'John Doe',
      givenName: 'John',
      familyName: 'Doe',
      gender: Gender.MALE,
      email: 'john@example.com',
      website: 'johndoe.com',
    }),
    new Organization({
      id: '',
      displayName: 'Facebook',
      databaseId: '2',
    }),
  ];

  return connectionFromArray(nodes, args);
}
