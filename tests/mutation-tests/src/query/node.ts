import { GraphQLError } from 'graphql';
import { GlobalId, ID, Node } from '@cortical/types';
import { resolveUser } from './user';

export async function resolveNode({ id }: { id: ID }): Promise<Node> {
  const type = GlobalId.getType(id);
  switch (type) {
    case 'User':
      return await resolveUser({ id });
  }
  throw new GraphQLError(`Could not resolve node with the specified id`);
}
