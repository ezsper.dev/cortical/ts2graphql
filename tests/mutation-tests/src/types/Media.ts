import { Resolver } from '../../../..';
import { ID, GlobalId, FQDN, URI, ObjectType, Node } from '@cortical/types';
import { GraphQLResolveInfo } from 'graphql';

function resolve<T, A extends {} | void = void, C extends any = any>(handler: Resolver<T, A, C>) {
  return handler;
}

export class MediaFields {
  /**
   * @GraphQL.disable
   */
  databaseId: string;
  uri: URI;
  childrenIds: ID[];
  children?: Media[];
  isVerified: false;
}

/**
 * @graphql.interface
 */
export class Media extends ObjectType(MediaFields) implements Node {

  // tslint:disable-next-line
  static Id = GlobalId.sign('Media', {
    databaseId: 'String',
  });

  get host(): FQDN {
    return '';
  }

  id: ID = Media.Id.stringify(this);

  get resolveChildren(): Resolver<Media[]> {
    return (args, context, info) => {
      return <any>[];
    };
  }

}