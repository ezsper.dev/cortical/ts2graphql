import { Resolver } from '../../../..';
import { ObjectType } from '@cortical/types';
import { Media, MediaFields } from './Media';

export class InstagramMediaFields extends MediaFields {
  instagramShortcode: string;
  children?: InstagramMedia[];
}

export class InstagramMedia extends ObjectType.extend(Media, InstagramMediaFields) {
  host = 'instagram.com';

  get resolveChildren(): Resolver<InstagramMedia[]> {
    return () => <any>[];
  }
}