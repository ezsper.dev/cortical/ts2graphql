import { Resolver } from '../../../..';
import { Gender } from './Gender';
import { Email } from './Email';
import { URL } from './URL';
import { Actor } from './Actor';
import {
  ID,
  GlobalId,
  ObjectType,
} from '@cortical/types';

export class UserFields {
  /**
   * The user's node id
   */
  id?: ID;
  /**
   * The user's database id
   * @GraphQL.disable
   */
  databaseId: string;
  /**
   * The user display name
   */
  displayName: string;
  /**
   * The user given / first name
   */
  givenName: string;
  /**
   * The user family / last name
   */
  familyName: string;
  /**
   * The user gender
   */
  gender: Gender;
  /**
   * The user email
   * @Graphql.nullable
   */
  email: Email;
  /**
   * The user metas
   * @GraphQL.disable
   */
  metas?: any;
  /**
   * The user website
   */
  website?: URL;

  friends?: User[];
}

/**
 * The User
 */
export class User extends ObjectType(UserFields) implements Actor {

  get id(): ID {
    return GlobalId.stringify('User', 'String', this.databaseId);
  }

  resolveFriends: Resolver<User[]> =
    async (): Promise<User[]> => ([]);

}
