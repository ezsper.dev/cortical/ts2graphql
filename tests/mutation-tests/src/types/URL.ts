/**
 * @GraphQL.sanitize text
 * @GraphQL.sanitize url
 * @GraphQL.validate url
 */
export type URL = string;
