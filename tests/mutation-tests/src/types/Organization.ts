import { Resolver } from '../../../..';
import { Actor } from './Actor';
import { ID, GlobalId, ObjectType } from '@cortical/types';
import { User } from './User';

export class OrganizationFields {

  id: ID;

  displayName: string;

  /**
   * @GraphQL.disable
   */
  databaseId: string;

  friends?: User[];

}

export class Organization extends ObjectType(OrganizationFields) implements Actor {

  resolveFriends: Resolver<User[]> =
    async (): Promise<User[]> => ([]);

}
