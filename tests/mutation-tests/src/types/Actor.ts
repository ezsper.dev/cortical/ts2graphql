import { Resolver } from '../../../..';
import { Node } from '@cortical/types';
import { User } from '../types/User';

/**
 * Represents an object which can take actions on the App.
 */
export interface Actor extends Node {
  displayName: string;
  friends?: User[];
  resolveFriends: Resolver<User[]>;
}
