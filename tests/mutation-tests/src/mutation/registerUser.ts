import { Resolver } from '../../../..';
import { User } from '../types/User';
import { Gender } from '../types/Gender';
import { Email } from '../types/Email';
import { URL } from '../types/URL';

/**
 * The User Registration Input
 */
export interface Arguments {
  /**
   * The User Registration Input
   */
  input: {
    clientMutationId?: string;
    /**
     * The user given / first name
     * @GraphQL.sanitize text
     * @GraphQL.validate min 3
     * @GraphQL.validate max 50
     */
    givenName: string;
    /**
     * The user family / last name
     * @GraphQL.sanitize text
     * @GraphQL.validate min 3
     * @GraphQL.validate max 50
     */
    familyName: string;
    /**
     * The user gender
     * @GraphQL.default "OTHER"
     */
    gender: Gender;
    /**
     * The user email
     */
    email: Email;
    /**
     * The user website
     * @GraphQL.nullable
     */
    website: URL;
  }
}

export interface Payload {
  clientMutationId?: string;
  user: User;
}

export const resolve: Resolver<Payload, Arguments> = async args => {
  const {
    clientMutationId,
    ...rest
  } = args.input;

  const user = new User({
    ...rest,
    databaseId: '1',
    displayName: 'John Doe',
  });

  return {
    clientMutationId,
    user,
  };
}
