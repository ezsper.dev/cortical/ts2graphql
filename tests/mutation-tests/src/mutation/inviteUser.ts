import { Resolver } from '../../../..';

export interface Entry {
  /**
   * The user email
   */
  email: string;
  /**
   * The user given / first name
   */
  givenName?: string;
  /**
   * The user family / last name
   */
  familyName?: string;
};

export interface Arguments {
  input: {
    clientMutationId?: string;
    entries: Entry[];
  }
}

export interface Result {
  clientMutationId?: string;
}

export const inviteUser: Resolver<Result, Arguments> = args => {
  const { clientMutationId } = args.input;
  return { clientMutationId };
};
