import { ValidatorMap } from '../../../..';
import { GraphQLError } from 'graphql';

export const validators: ValidatorMap<string> = {
  sanitize: {
    text() {
      return value => value.trim().replace(/\s+/g, ' ');
    },
    url() {
      return value => value.replace(/^((http)(s)?:\/\/)?(.+)/, 'http$3://$4');
    },
  },
  validate: {
    min(num: number) {
      return value => {
        if (value.length < num) {
          throw new GraphQLError(`It must be at least ${num} characters`);
        }
      };
    },
    max(num: number) {
      return value => {
        if (value.length > num) {
          throw new GraphQLError(`It must be at maximum ${num} characters`);
        }
      };
    },
    url() {
      return value => {
        if (!value.match(/^https?:\/\//)) {
          console.log(value);
          throw new GraphQLError(`It must be a URL`);
        }
      };
    }
  },
};
