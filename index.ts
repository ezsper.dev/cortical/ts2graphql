import {
  resolve as pathResolve,
  relative as pathRelative,
  basename,
  dirname,
} from 'path';
import * as appRootDir from 'app-root-dir';
import * as fs from 'fs';
import { printType } from 'graphql/utilities/schemaPrinter';
import { SpawnSyncReturns } from 'child_process';
import { createTypeDocJson, Handler as CreateTypeDocJsonHandler } from './utils/createTypeDocJson';
import {
  TypeDoc2GraphQLOptions,
  TypeDoc2GraphQLPaths,
  TypeDoc2GraphQL,
  Parser,
  ParserRule,
} from './utils/TypeDoc2GraphQL';
import {
  addResolveFunctionsToSchema,
} from 'graphql-tools';
import {
  IResolvers,
  IResolverObject,
  IResolverValidationOptions,
} from 'graphql-tools/dist/Interfaces';
import { pathSync } from './utils/pathSync';
import { capitalize } from './utils/capitalize';
import {
  GraphQLObjectType,
  GraphQLUnionType,
  GraphQLInterfaceType,
  GraphQLScalarType,
  GraphQLInputObjectType,
  GraphQLEnumType,
  GraphQLNamedType,
  GraphQLSchema,
  GraphQLInt,
  GraphQLString,
  GraphQLFloat,
  GraphQLBoolean,
  GraphQLNonNull,
  GraphQLType,
  buildSchema,
  GraphQLResolveInfo,
  GraphQLError,
} from 'graphql';
import { GlobSync } from 'glob';
import { exec } from './utils/exec';
import { Validator } from './utils/Validator';
import { isAsyncIterable } from 'iterall';

export * from './utils/TypeDoc2GraphQL';
export * from './utils/Validator';

export interface SourceMapField {
  sanitize?: Parser[];
  validate?: Parser[];
  sanitizeArgs?: {[arg: string]: Parser[]};
  validateArgs?: {[arg: string]: Parser[]};
}

export interface SourceMap {
  name: string;
  type: string;
  fields?: {[field: string]: SourceMapField};
  values?: {[key: string]: number | string};
}

export interface TypeScript2GraphQLPaths extends TypeDoc2GraphQLPaths {
  root: string;
  validators: string;
  scalars: string;
}

export interface TypeScript2GraphQLOptions extends TypeDoc2GraphQLOptions {
  paths?: Partial<TypeDoc2GraphQLPaths> & Partial<TypeScript2GraphQLPaths>;
}

export type Resolver<T, A extends {} | void = void, C extends any = any> =
  (args: A, context: C, info?: GraphQLResolveInfo) => T | Promise<T>;

export type AsyncIteratorSubscriber<T, A extends {} | void = void, C extends any = any> =
  (args: A, context: C, info?: GraphQLResolveInfo) => AsyncIterator<T> | AsyncIterableIterator<T>;

export namespace Observable {
  export interface Disposable {
    dispose(): void;
  }

  export interface Observable<T> {
    subscribe(onNext?: (value: T) => void, onError?: (exception: any) => void, onCompleted?: () => void): Disposable;
  }
}

export type ObservableSubscriber<T, A extends {} | void = void, C extends any = any> =
  (args: A, context: C, info?: GraphQLResolveInfo) => Observable.Observable<T>;

export type Subscriber<T, A extends {} | void = void, C extends any = any> = AsyncIteratorSubscriber<T, A, C> | ObservableSubscriber<T, A, C>;
export type SubscriptionResolver<T, A extends {} | void = void, C extends any = any> = Resolver<T, A, C>;

export async function graphqlParseOutput(value: any, context: any, info: any): Promise<any> {
  if (Array.isArray(value)) {
    return await Promise.all(value.map(item => graphqlParseOutput(item, context, info)));
  }
  if (value instanceof Promise) {
    return value.then(promisedValue => graphqlParseOutput(promisedValue, context, info));
  }
  if (typeof value === 'object' && value != null) {
    let constructor: any;
    if (typeof value.toGraph === 'function') {
      constructor = value.constructor;
      value = await value.toGraph(context, info);
    }
    if (value instanceof Date) {
      return value;
    } else if (typeof value.toJSON === 'function') {
      value = value.toJSON();
    } else if (typeof value.toJSDate === 'function') {
      return value.toJSDate();
    } else if (typeof value.toString === 'function'
      && value.toString !== Object.prototype.toString) {
      return value.toString();
    } else if (typeof value.toNumber === 'function') {
      return value.toNumber();
    }
    // TODO: Add Set and Map handlers
    if (typeof value === 'object' && value != null) {
      const obj: any = {};
      if (typeof obj.__typename === 'undefined') {
        if (typeof constructor !== 'undefined' && typeof constructor.graphqlType !== 'undefined') {
          obj.__typename = constructor.graphqlType.name;
        }
      }
      for (const key of Object.keys(value)) {
        obj[key] = await graphqlParseOutput(value[key], context, info);
      }
      return obj;
    }
    return value;
  }
  return value;
}

export function getValidator(rootPath: string, validatorDir: string): Validator {
  const validatorsPath = pathResolve(rootPath, validatorDir);
  const glob = new GlobSync('*.@(ts|js)', {
    ignore: '*.d.ts',
    cwd: validatorsPath,
  });
  const validatorTypeMap: any = {};
  for (const file of glob.found) {
    const fileName = pathResolve(validatorsPath, file);
    const typeName = basename(fileName).replace(/\.(ts|js)$/, '');
    if (typeName === 'index') {
      continue;
    }
    const validatorFile = require(fileName);
    if (!('validators' in validatorFile)) {
      throw new Error(`The file ${pathRelative(rootPath, fileName)} must export validators`);
    }
    const { validators } = validatorFile;
    if (!('sanitize' in validators)) {
      throw new Error(`The file ${pathRelative(rootPath, fileName)} must export validators.sanitize`);
    }
    if (!('validate' in validators)) {
      throw new Error(`The file ${pathRelative(rootPath, fileName)} must export validators.validate`);
    }
    validatorTypeMap[typeName] = validators;
  }
  return new Validator(validatorTypeMap);
}

export function getScalars(rootPath: string, scalarsDir: string): GraphQLNamedType[] {
  const scalarsPath = pathResolve(rootPath, scalarsDir);
  const glob = new GlobSync('*.@(ts|js)', {
    ignore: '*.d.ts',
    cwd: scalarsPath,
  });
  const scalars: GraphQLNamedType[] = [];
  for (const file of glob.found) {
    const fileName = pathResolve(scalarsPath, file);
    const graphqlTypeName = basename(fileName).replace(/\.(ts|js)$/, '');
    if (graphqlTypeName === 'index') {
      continue;
    }
    const nameMatch = graphqlTypeName.match(/^GraphQL(.*)/);
    if (nameMatch === null) {
      throw new Error(`The file ${pathRelative(rootPath, fileName)} must start with GraphQL name`);
    }
    const scalarFile = require(fileName);
    if (!(graphqlTypeName in scalarFile)) {
      throw new Error(`The file ${pathRelative(rootPath, fileName)} must export ${nameMatch[1]}`);
    }
    const { [graphqlTypeName]: type } = scalarFile;
    if (!(type instanceof GraphQLScalarType
      || type instanceof GraphQLObjectType
      || type instanceof GraphQLInputObjectType
      || type instanceof GraphQLInterfaceType
      || type instanceof GraphQLUnionType
    )) {
      throw new Error(`The file ${pathRelative(rootPath, fileName)} must export a named type`);
    }
    if (type.name !== nameMatch[1]) {
      throw new Error(`The file ${pathRelative(rootPath, fileName)} must export a type named ${nameMatch[1]}`);
    }
    scalars.push(type);
  }
  return scalars;
}

export class TypeScript2GraphQL extends TypeDoc2GraphQL {

  protected rootPath: string;
  protected paths: TypeScript2GraphQLPaths;

  constructor(options: TypeScript2GraphQLOptions = {}) {
    const { types, parserHandler } = options;
    const paths = {
      root: appRootDir.get(),
      types: 'types',
      query: 'query',
      mutation: 'mutation',
      subscription: 'subscription',
      validators: 'validators',
      scalars: 'scalars',
      ...options.paths,
    };
    let restPaths: any;
    let rootPath: string;
    ({ root: rootPath, ...restPaths } = paths);
    const stats = fs.lstatSync(rootPath);
    if (!stats.isDirectory()) {
      throw new Error('The root path must be a directory');
    }
    for (const name of Object.keys(restPaths)) {
      if (restPaths[name] !== undefined) {
        restPaths[name] = pathResolve(rootPath, restPaths[name]);
      }
    }
    super({
      types: (types !== undefined ? types : []).concat(getScalars(rootPath, restPaths.scalars)),
      parserHandler: parserHandler !== undefined ? parserHandler : getValidator(rootPath, restPaths.validators),
      paths: restPaths,
    });
    this.rootPath = rootPath;
  }

  generate(handler?: CreateTypeDocJsonHandler, stdio: any = 'inherit') {
    if (this.typedoc === undefined) {
      const typedoc = createTypeDocJson(
        this.rootPath,
        handler,
        stdio,
      );
      this.initialize(typedoc);
    }
  }

  restoreSourceMap(type: GraphQLNamedType, map: SourceMap) {
    if (type instanceof GraphQLEnumType && map.values !== undefined) {
      const typeValues = type.getValues();
      for (const index in typeValues) {
        const value = typeValues[index];
        typeValues[index].value = map.values[value.name];
      }
    } else if (type instanceof GraphQLInputObjectType && map.fields !== undefined) {
      const fields = type.getFields();
      for (const key of Object.keys(fields)) {
        if (Object.prototype.hasOwnProperty.call(fields, key)) {
          if (!map.fields.hasOwnProperty(key)) {
            continue;
          }
          if (map.fields[key].hasOwnProperty('validate')) {
            (<any>fields[key]).validate = map.fields[key].validate;
          }
          if (map.fields[key].hasOwnProperty('sanitize')) {
            (<any>fields[key]).sanitize = map.fields[key].sanitize;
          }
        }
      }
    } else if (type instanceof GraphQLObjectType && map.fields !== undefined) {
      const fields = type.getFields();
      for (const key of Object.keys(fields)) {
        if (!Object.prototype.hasOwnProperty.call(fields, key)) {
          continue;
        }
        const { args } = fields[key];
        if (args === undefined) {
          continue;
        }
        for (const index in args) {
          const argName = args[index].name;
          if (!map.hasOwnProperty('fields')
            || !map.fields.hasOwnProperty(key)
            || map.fields[key].validateArgs === undefined
          ) {
            continue;
          }
          const validateArgs = (<any>map.fields[key].validateArgs);
          if (validateArgs.hasOwnProperty(argName)) {
            (<any>args[index]).validate = validateArgs[argName];
          }
        }
      }
    }
  }

  restoreSchema(inputDir: string = 'schema') {
    const glob = new GlobSync('**/*.graphql', {
      cwd: inputDir,
    });

    const defs = [];
    const maps = [];

    for (const file of glob.found) {
      defs.push(
        fs.readFileSync(pathResolve(inputDir, file)).toString(),
      );
      const mapJsonFile = pathResolve(inputDir, file.replace(/\.graphql$/, '.map.json'));
      if (fs.existsSync(mapJsonFile)) {
        maps.push(JSON.parse(fs.readFileSync(mapJsonFile).toString()));
      }
    }
    const schema = buildSchema(defs.join('\n\n'));
    for (const map of maps) {
      const type = schema.getType(map.name);
      if (type != null) {
        this.restoreSourceMap(type, map);
      }
    }
    return schema;
  }

  dumpSchema(outputDir: string = 'schema') {
    const schema= this.getSchema();
    const singleFileMatch = outputDir.match(/([^\/]+)\.graphql$/);
    let singleFile: string = '';
    if (singleFileMatch !== null) {
      // tslint:disable-next-line no-parameter-reassignment
      outputDir = dirname(outputDir);
      if (outputDir === singleFileMatch[0]) {
        // tslint:disable-next-line no-parameter-reassignment
        outputDir = '.';
      }
      singleFile = singleFileMatch[1];
    }
    const outputPath = pathResolve(this.rootPath, outputDir);

    pathSync(outputPath);

    if (singleFileMatch === null && outputPath !== this.rootPath) {
      exec(`node_modules/.bin/rimraf ${outputPath}`);
      try {
        fs.mkdirSync(outputPath);
      } catch (err) {}
    }

    const types = [
      schema.getQueryType(),
      schema.getMutationType(),
      schema.getSubscriptionType(),
      ...Object.values(schema.getTypeMap())
    ];
    const parts = [];
    for (const type of types) {
      if (type == null) {
        continue;
      }
      if (type instanceof GraphQLObjectType
        || type instanceof GraphQLInputObjectType
        || type instanceof GraphQLEnumType
        || type instanceof GraphQLScalarType
        || type instanceof GraphQLInterfaceType
      ) {
        if (singleFileMatch === null) {
          fs.writeFileSync(
            pathResolve(outputPath, `${type.name}.graphql`),
            this.getSource(type),
          );

          const map = this.getSourceMap(type);

          if (map !== undefined) {
            fs.writeFileSync(
              pathResolve(outputPath, `${type.name}.map.json`),
              JSON.stringify(map, null, 2),
            );
          }
        } else {
          parts.push(this.getSource(type));
        }
      } else {
        const name = type.name === undefined ? type.constructor.name : type.name;
        console.log(`skipping ${name} as it cannot be resolved to source`);
      }
    }

    if (singleFileMatch !== null) {
      fs.writeFileSync(
        pathResolve(outputPath, `${singleFile}.graphql`),
        parts.join('\n\n'),
      );
    }
  }

  getSourceMap(type: any): void | SourceMap {
    let map: any;
    if (type instanceof GraphQLEnumType) {
      GraphQLEnumType.name
      map = {
        name: type.name,
        type: 'enum',
        values: {},
      };
      for (const value of type.getValues()) {
        map.values[value.name] = value.value;
      }
    } else if (type instanceof GraphQLInputObjectType) {
      const fields = type.getFields();
      const createMap = () => {
        if (map === undefined) {
          map = {
            name: type.name,
            type: 'input',
            fields: {},
          };
        }
      };
      const createFieldMap = (key: string) => {
        createMap();
        if (!map.fields.hasOwnProperty(key)) {
          map.fields[key] = {};
        }
      }
      for (const key of Object.keys(fields)) {
        if (!Object.prototype.hasOwnProperty.call(fields, key)) {
          continue;
        }
        if (fields[key].hasOwnProperty('validate')) {
          createFieldMap(key);
          map.fields[key].validate = (<any>fields[key]).validate;
        }
        if (fields[key].hasOwnProperty('sanitize')) {
          createFieldMap(key);
          map.fields[key].sanitize = (<any>fields[key]).sanitize;
        }
      }
    } else if (type instanceof GraphQLObjectType) {
      const fields = type.getFields();
      const createMap = () => {
        if (map === undefined) {
          map = {
            name: type.name,
            type: 'input',
            fields: {},
          };
        }
      };
      const createFieldMap = (key: string) => {
        createMap();
        if (!map.fields.hasOwnProperty(key)) {
          map.fields[key] = {};
        }
      }
      const createFieldValidateArgMap = (key: string) => {
        createFieldMap(key);
        if (!map.fields[key].hasOwnProperty('validateArgs')) {
          map.fields[key].validateArgs = {};
        }
      }
      const createFieldSanitizeArgMap = (key: string) => {
        createFieldMap(key);
        if (!map.fields[key].hasOwnProperty('sanitizeArgs')) {
          map.fields[key].sanitizeArgs = {};
        }
      }
      for (const key of Object.keys(fields)) {
        if (Object.prototype.hasOwnProperty.call(fields, key)) {
          const field = fields[key];
          for (const arg of field.args) {
            if (arg.hasOwnProperty('validate')) {
              createFieldValidateArgMap(key);
              map.fields[key].validateArgs[arg.name] = (<any>arg).validate;
            }
            if (arg.hasOwnProperty('sanitize')) {
              createFieldSanitizeArgMap(key);
              map.fields[key].sanitizeArgs[arg.name] = (<any>arg).sanitize;
            }
          }
        }
      }
    }
    return map;
  }

  injectParserHandlerOnObjectType(type: GraphQLObjectType) {
    const fields = type.getFields();
    for (const fieldName of Object.keys(fields)) {
      if (!Object.prototype.hasOwnProperty.call(fields, fieldName)) {
        continue;
      }
      const field = fields[fieldName];
      const resolveFunction = field.resolve;
      const sanitizeRules: ParserRule[] = [];
      const validateRules: ParserRule[] = [];
      for (const arg of field.args) {
        const sanitize = <Parser[]>(<any>arg).sanitize;
        if (sanitize !== undefined) {
          for (const parser of sanitize) {
            sanitizeRules.push({
              path: [arg.name],
              valueType: arg.type,
              ...parser,
            });
          }
        }
        const validate = <Parser[]>(<any>arg).validate;
        if (validate !== undefined) {
          for (const parser of validate) {
            validateRules.push({
              path: [arg.name],
              valueType: arg.type,
              ...parser,
            });
          }
        }
        const parseInputObjectTypeArg = (type: GraphQLType, path: string[]) => {
          if (type instanceof GraphQLNonNull && 'ofType' in type) {
            // tslint:disable-next-line no-parameter-reassignment
            type = (<any>type).ofType;
          }
          if (!(type instanceof GraphQLInputObjectType)) {
            return;
          }
          const input = type;
          const inputFields = input.getFields();
          for (const inputKey of Object.keys(inputFields)) {
            if (!Object.prototype.hasOwnProperty.call(inputFields, inputKey)) {
              continue;
            }
            const inputField = inputFields[inputKey];
            const sanitize = <Parser[]>(<any>inputField).sanitize;
            if (sanitize !== undefined) {
              for (const parser of sanitize) {
                sanitizeRules.push({
                  path: path.concat(inputKey),
                  valueType: inputField.type,
                  ...parser,
                });
              }
            }
            const validate = <Parser[]>(<any>inputField).validate;
            if (validate !== undefined) {
              for (const parser of validate) {
                validateRules.push({
                  path: path.concat(inputKey),
                  valueType: inputField.type,
                  ...parser,
                });
              }
            }
            if (inputField.type instanceof GraphQLInputObjectType) {
              parseInputObjectTypeArg(inputField.type, path.concat(inputKey));
            }
          }
        }
        parseInputObjectTypeArg(arg.type, [arg.name]);
      }
      if (sanitizeRules.length === 0 && validateRules.length === 0) {
        continue;
      }
      field.resolve = async (source, args, context, info) => {
        await this.parserHandler.parse({
          type,
          fieldName,
          args,
          sanitizeRules,
          validateRules,
          source,
          context,
          info,
        });
        if (resolveFunction !== undefined) {
          return await resolveFunction(source, args, context, info);
        }
        return null;
      };
    }
  }

  injectParserHandler(schema: GraphQLSchema) {
    const typeMap = schema.getTypeMap();
    for (const name of Object.keys(typeMap)) {
      const type = typeMap[name];
      if (type instanceof GraphQLObjectType) {
        this.injectParserHandlerOnObjectType(type);
      }
    }
  }

  addResolveFunctionsToSchema(
    schema: GraphQLSchema,
    resolvers: IResolvers,
    resolverValidationOptions?: IResolverValidationOptions,
  ) {
    addResolveFunctionsToSchema({
        schema,
        resolvers,
        resolverValidationOptions: {
          ...resolverValidationOptions,
          /*
           * This option disables warning for GraphQL objects without resolveType
           * as Union and other types do not have it
           * TODO: Implement those methods
           */
          requireResolversForResolveType: false,
        },
    })
    this.injectParserHandler(schema);
  }

  getSource(type: any) {
    return printType(type);
  }

  getMethodResolvers(dir: string, name: string): IResolvers {
    const resolvers: IResolvers & {[key: string]: IResolverObject} = {};
    const methodPath = pathResolve(this.rootPath, dir);
    const glob = new GlobSync('*.@(ts|js)', {
      ignore: ['*.d.ts'],
      cwd: methodPath,
    });
    for (const file of glob.found) {
      const fileName = pathResolve(methodPath, file);
      const methodName = basename(fileName).replace(/\.(ts|js)$/, '');
      if (methodName === 'index') {
        continue;
      }
      if (!resolvers.hasOwnProperty(name)) {
        resolvers[name] = {};
      }
      const exports = require(fileName);
      let resolver: any;
      const upperCased = methodName.charAt(0).toUpperCase() + methodName.slice(1);
      if (methodName in exports) {
        resolver = exports[methodName];
      } else {
        if (name === 'Subscription') {
          resolver = exports[`subscribe${upperCased}`];
          if (typeof resolver === 'undefined') {
            resolver = exports.subscribe;
          }
        } else {
          resolver = exports[`resolve${upperCased}`];
          if (typeof resolver === 'undefined') {
            resolver = exports.resolve;
          }
        }
      }
      if (typeof resolver === 'undefined') {
        throw new Error(`Couldn't find an exported variable for ${name}.${methodName}`);
      }

      const mappedResolver = (obj: any, args: any[], context: any, info: any) =>
        resolver(args, context, info);
      if (name === 'Subscription') {
        resolvers[name][methodName] = {
          resolve: (obj: any, args: any[], context: any, info: any) => {
            if (obj === undefined) {
              throw new GraphQLError('You must execute subscription under WebSocket');
            }
            return graphqlParseOutput(obj[methodName], context, info);
          },
          subscribe: (obj: any, args: any[], context: any, info: any) => {
            const iterable = mappedResolver(obj, args, context, info);
            if ('subscribe' in iterable) {
              return (async function *() {
                function nextResolver() {
                  const resolver: any = {};
                  resolver.promise = new Promise((resolve, reject) => {
                    resolver.resolve = resolve;
                    resolver.reject = reject;
                  });
                  return resolver;
                }
                const buffer = [nextResolver()];
                const subscription = iterable.subscribe(
                  (value: any) => {
                    const { resolve } = buffer[buffer.length - 1];
                    buffer.push(nextResolver());
                    resolve({ value });
                  },
                  (error: Error) => {
                    resolver.reject(error);
                  },
                  (value: any) => {
                    const { resolve } = buffer[buffer.length - 1];
                    buffer.push(nextResolver());
                    resolve({ value, done: true });
                  },
                );
                try {
                  while (true) {
                    const { value, done } = await (buffer[0].promise);
                    buffer.shift();
                    if (done === true) {
                      return value !== undefined ? { [methodName]: value } : value;
                    }
                    yield { [methodName]: value };
                  }
                } finally {
                  subscription.dispose();
                  while(buffer.shift()) {}
                }
              })();
            }
            if (isAsyncIterable(iterable)) {
              return (async function *() {
                for await (const result of iterable) {
                  yield { [methodName]: result };
                }
              })();
            }
            throw new Error(`Could not resolve subscribe method ${name}.${methodName}, did not return AsyncIterator or Observable`);
          },
        };
      } else {
        resolvers[name][methodName] = (obj: any, args: any[], context: any, info: any) =>
          graphqlParseOutput(mappedResolver(obj, args, context, info), context, info);
      }
    }
    return resolvers;
  }

  getTypeResolvers(schema: GraphQLSchema): IResolvers {
    const resolvers: IResolvers = {};
    const methodPath = pathResolve(this.rootPath, 'types');
    const glob = new GlobSync('*.@(ts|js)', {
      ignore: '*.d.ts',
      cwd: methodPath,
    });
    for (const file of glob.found) {
      const fileName = pathResolve(methodPath, file);
      const typeName = basename(fileName).replace(/\.(ts|js)$/, '');
      if (typeName === 'index') {
        continue;
      }
      const type = require(fileName)[typeName];
      const graphqlType = schema.getType(typeName);
      if (graphqlType instanceof GraphQLObjectType || (type != null && graphqlType instanceof GraphQLInterfaceType)) {
        Object.defineProperty(type, 'graphqlType', { value: graphqlType });
        Object.defineProperty(type, 'originalType', { value: type });
        if (graphqlType instanceof GraphQLObjectType) {
          if (typeof type.isTypeOf === 'function') {
            graphqlType.isTypeOf = (value: any, context: any, info: GraphQLResolveInfo) => {
              return type.isTypeOf(value, context, info);
            };
          }
        }
      }
      if (typeof type === 'function') {
        const resolveObject: IResolverObject = {};
        const { graphqlType } = type;
        if (typeof graphqlType !== 'undefined') {
          const fields = graphqlType.getFields();
          for (const fieldName of Object.keys(fields)) {
            resolveObject[fieldName] =
              (obj, args: any[], context: any, info: any) => {
                let result: any = null;
                const methodName = `resolve${capitalize(fieldName)}`;
                if (typeof obj[methodName] === 'function') {
                  result = obj[methodName](args, context, info);
                } else if (obj[fieldName] != null) {
                  result = obj[fieldName];
                }
                return graphqlParseOutput(result, context, info);
              };
          }
          resolvers[graphqlType.name] = resolveObject;
        }
      }
    }
    return resolvers;
  }

  getResolvers(schema: GraphQLSchema): IResolvers {
    return {
      ...this.getTypeResolvers(schema),
      ...this.getMethodResolvers('query', 'Query'),
      ...this.getMethodResolvers('mutation', 'Mutation'),
      ...this.getMethodResolvers('subscription', 'Subscription'),
    };
  }

  getSchemaConfig(handler?: CreateTypeDocJsonHandler, stdio: any = 'inherit') {
    this.generate(handler);
    return super.getSchemaConfig();
  }

  getSchema() {
    const schema = super.getSchema();
    return schema;
  }

  getSchemaWithResolvers() {
    const schema = this.getSchema();
    const resolvers = this.getResolvers(schema);
    this.addResolveFunctionsToSchema(schema, resolvers);
    return schema;
  }

  restoreSchemaWithResolvers(inputDir: string = 'schema') {
    const schema = this.restoreSchema(inputDir);
    const resolvers = this.getResolvers(schema);
    this.addResolveFunctionsToSchema(schema, resolvers);
    return schema;
  }

}
