/**
 * This script builds a production output of all of our bundles.
 */

import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs';
import { builder, handler, Arguments } from './cli.yargs';
import { TypeScript2GraphQL } from '..';

export default async (argv: Arguments) => {
  const rootPath = pathResolve(process.cwd(), argv.project);
  const transpiler = new TypeScript2GraphQL({
    paths: {
      root: rootPath,
      types: argv.typesDir,
      query: argv.queryDir,
      mutation: argv.mutationDir,
      subscription: argv.subscriptionDir,
      validators: argv.validatorsDir,
      scalars: argv.scalarsDir,
    },
  });
  transpiler.dumpSchema(pathResolve(process.cwd(), argv.output));
}

export async function exec() {
  try {
    await handler(builder().argv);
  } catch(err) {
    console.log(err.stack !== undefined ? err.stack : err.message);
  }
}

if (!module.parent) {
  exec();
}
