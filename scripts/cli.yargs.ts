import * as Yargs from 'yargs';

export interface Arguments extends Yargs.Arguments {
  project: string;
  typesDir: string;
  mutationDir: string;
  queryDir: string;
  subscriptionDir: string;
  validatorsDir: string;
  scalarsDir: string;
  output: string;
}

export interface Argv extends Yargs.Argv {
  argv: Arguments;
}

export const builder = (yargs: Yargs.Argv = Yargs) =>
  <Argv>yargs
    .option('output', {
      alias: 'o',
      describe: 'Output path to store generated .graphql files',
      type: 'string',
      default: 'schema'
    })
    .option('project', {
      alias: 'p',
      describe: 'The project path to look for directories',
      type: 'string',
      default: '.',
    })
    .option('types-dir', {
      alias: 't',
      describe: 'The types dir where single types are located',
      type: 'string',
      default: './types',
    })
    .option('mutation-dir', {
      alias: 'm',
      describe: 'The mutation dir where single methods are located',
      type: 'string',
      default: './mutation',
    })
    .option('query-dir', {
      alias: 'q',
      describe: 'The query dir where single methods are located',
      type: 'string',
      default: './query',
    })
    .option('subscription-dir', {
      alias: 's',
      describe: 'The subscription dir where single methods are located',
      type: 'string',
      default: './subscription',
    })
    .option('validators-dir', {
      alias: 's',
      describe: 'The dir where type validators stays',
      type: 'string',
      default: './validators',
    })
    .option('scalars-dir', {
      alias: 's',
      describe: 'The dir where graphql scalars are located',
      type: 'string',
      default: './scalars',
    })

export const handler = async (argv: Arguments) => {
  await (await import('./cli')).default(argv);
}
