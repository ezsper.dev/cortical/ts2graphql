/**
 * This script builds to distribuition
 */
import * as appRootDir from 'app-root-dir';
import { resolve as pathResolve } from 'path';
import * as fs from 'fs-extra';
import { builder, handler, Arguments } from './release.yargs';
import { exec } from '../utils/exec';

function fileFilter(
  target: string,
  filter: (data: string) => string,
  encoding = 'utf8',
) {
  fs.writeFileSync(
    target,
    filter(fs.readFileSync(target).toString(encoding)),
    encoding,
  );
}

export default async (argv: Arguments) => {
  // First clear the build output dir.
  exec(`rimraf ${pathResolve(appRootDir.get(), 'dist')}`);
  // Build typescript
  exec('tsc');
  // Copy documents
  fs.copySync(
    pathResolve(appRootDir.get(), 'README.md'),
    pathResolve(appRootDir.get(), 'dist/README.md'),
  );
  // Copy bin
  fs.copySync(
    pathResolve(appRootDir.get(), 'bin'),
    pathResolve(appRootDir.get(), 'dist/bin'),
  );
  // replace ts-node for node
  fileFilter(
    pathResolve(appRootDir.get(), 'dist/bin/ts2graphql'),
    data => data.replace(/.*ts-node\/register.*/, ''),
  );
  // Clear our package json for release
  const packageJson = require('../package.json');
  packageJson.main = './index.js';
  delete packageJson.scripts;
  delete packageJson['lint-staged'];
  delete packageJson.jest;
  delete packageJson.devDependencies;

  fs.writeFileSync(
    pathResolve(appRootDir.get(), './dist/package.json'),
    JSON.stringify(packageJson, null, 2),
  );
  // Copy .gitignore
  exec('cp .gitignore dist/');
}

if (!module.parent) {
  handler(builder().argv)
    .catch(err => {
      console.log(err.stack);
    });
}
