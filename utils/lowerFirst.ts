export function lowerFirst(value: string) {
  return value.charAt(0).toLowerCase() + value.slice(1)
}
