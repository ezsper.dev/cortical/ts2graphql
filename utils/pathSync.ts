import * as fs from 'fs';
import * as path from 'path';

export function pathSync(targetDir: string, { isRelativeToScript = false } = {}) {
  const sep = path.sep;
  const initDir = path.isAbsolute(targetDir) ? sep : '';
  const baseDir = isRelativeToScript ? __dirname : '.';

  targetDir.split(sep).reduce((parentDir, childDir) => {
    const curDir = path.resolve(baseDir, parentDir, childDir);
    try {
      fs.mkdirSync(curDir);
    } catch (err) {
      if (err.code !== 'EEXIST' && err.code !== 'EISDIR') {
        throw err;
      }
    }

    return curDir;
  }, initDir);
}
