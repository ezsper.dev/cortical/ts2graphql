export function values(obj: any): any[] {
  if (typeof Object.values !== 'undefined') {
    return Object.values(obj);
  }
  const values = [];
  for (const key of Object.keys(obj)) {
    values.push(obj[key]);
  }
  return values;
}
