import {
  ParserHandler,
  ParserHandlerParseOptions,
} from './TypeDoc2GraphQL';
import {
  ApolloError,
  ValidationError,
  fromGraphQLError,
} from 'apollo-server-errors';
import {
  GraphQLType,
  GraphQLNamedType,
  GraphQLNonNull,
  GraphQLList,
  GraphQLResolveInfo,
  GraphQLError,
} from 'graphql';
export type ValidatorParseHandler<T, R> = (
  value: T,
  source: any,
  context: any,
  info: GraphQLResolveInfo,
) => R | Promise<R>;
export type ValidatorHandler<T, R> = (...args: any[]) => ValidatorParseHandler<T, R>;

export type ValidatorSanitizeMap<T> = {[methodName: string]: ValidatorHandler<T, T>};
export type ValidatorValidateMap<T> = {[methodName: string]: ValidatorHandler<T, void>};

export interface ValidatorMap<T> {
  sanitize: ValidatorSanitizeMap<T>;
  validate: ValidatorValidateMap<T>;
}

export interface ValidatorTypeMap {
  [typeName: string]: ValidatorMap<any>
}

function findNamedType(type: GraphQLType): GraphQLNamedType {
  if ('ofType' in type) {
    return findNamedType(type.ofType);
  }
  return type;
}

function getPath(obj: {}, path: string[]): any {
  let result: any = obj;
  for (const part of path) {
    result = result[part];
    if (result === undefined) {
      break;
    }
  }
  return result;
}

function setPath(obj: {}, path: string[], value: any): void {
  let result: any = obj;
  const parts = path.slice(0, path.length - 1);
  const name = path[path.length - 1];
  for (const part of parts) {
    result = result[part];
    if (result === undefined) {
      return;
    }
  }
  result[name] = value;
}

export class Validator implements ParserHandler {

  private map: ValidatorTypeMap;

  constructor(map: ValidatorTypeMap = {}) {
    this.map = map;
  }

  getTypeSanitizer<T>(typeName: string, name: string): ValidatorHandler<T, T> | void {
    if (typeName in this.map && name in this.map[typeName].sanitize) {
      return this.map[typeName].sanitize[name];
    }
  }

  getTypeValidator<T>(typeName: string, name: string): ValidatorHandler<T, void> | void {
    if (typeName in this.map && name in this.map[typeName].validate) {
      return this.map[typeName].validate[name];
    }
  }

  getTypeAction<T>(typeName: string, action: 'sanitize' | 'validate', name: string) {
    if (action === 'sanitize') {
      return this.getTypeSanitizer<T>(typeName, name);
    }
    return this.getTypeValidator(typeName, name);
  }

  test(action: 'sanitize' | 'validate', valueType: GraphQLType, name: string, args: (boolean|string|number|null)[]) {
    const isNonNull = valueType instanceof GraphQLNonNull;
    const isList = (valueType instanceof GraphQLNonNull && 'ofType' in valueType)
      ? (<any>valueType).ofType instanceof GraphQLList
      : valueType instanceof GraphQLList;
    const type = findNamedType(valueType);
    const actionHandler = this.getTypeAction<any>(type.name, action, name);
    if (!actionHandler) {
      throw new Error(`Unknown ${action}.${name} method for ${type.name}`);
    }
    try {
      actionHandler(...args);
    } catch (error) {
      return error;
    }
  }

  async parse(options: ParserHandlerParseOptions) {
    const {
      args,
      source,
      context,
      info,
      fieldName,
      sanitizeRules,
      validateRules,
    } = options;

    for (const { valueType, path, name, args: methodArgs } of sanitizeRules) {
      try {
        const isNonNull = valueType instanceof GraphQLNonNull;
        const isList = (valueType instanceof GraphQLNonNull && 'ofType' in valueType)
          ? (<any>valueType).ofType instanceof GraphQLList
          : valueType instanceof GraphQLList;
        const type = findNamedType(valueType);
        const value = getPath(args, path);
        const method = this.getTypeSanitizer(type.name, name);
        if (value === undefined || !method) {
          continue;
        }
        let parsedValue: any;
        if (Array.isArray(value) && isList) {
          parsedValue = [];
          for (const item of value) {
            parsedValue.push(await (method(...methodArgs)(item, source, context, info)));
          }
        } else {
          parsedValue = await (method(...methodArgs)(value, source, context, info));
        }
        setPath(args, path, parsedValue);
      } catch (error) {
        if (error instanceof GraphQLError) {
          let errorPath: (string|number)[] = [];
          if (info.path !== undefined) {
            let { prev } = info.path;
            while (prev !== undefined) {
              errorPath.push(prev.key);
              prev = prev.prev;
            }
            errorPath.push(info.path.key);
          }
          errorPath = errorPath.concat(['$args'], path.slice(1));
          throw new GraphQLError(
            error.message,
            error.nodes !== undefined ? error.nodes : info.fieldNodes,
            error.source !== undefined ? error.source : source,
            error.positions,
            errorPath,
            error.originalError,
          );
        }
        throw error;
      }
    }


    for (const { valueType, path, name, args: methodArgs } of validateRules) {
      try {
        const isNonNull = valueType instanceof GraphQLNonNull;
        const isList = (valueType instanceof GraphQLNonNull && 'ofType' in valueType)
          ? (<any>valueType).ofType instanceof GraphQLList
          : valueType instanceof GraphQLList;
        const type = findNamedType(valueType);
        const value = getPath(args, path);
        const method = this.getTypeValidator(type.name, name);
        if (value === undefined || !method) {
          continue;
        }
        if (Array.isArray(value) && isList) {
          for (const item of value) {
            await (method(...methodArgs)(item, source, context, info));
          }
        } else {
          await (method(...methodArgs)(value, source, context, info));
        }
      } catch (error) {
        if ((error instanceof GraphQLError || error instanceof ApolloError)) {
          let errorPath: (string|number)[] = [];
          if (info.path !== undefined) {
            let { prev } = info.path;
            while (prev !== undefined) {
              errorPath.push(prev.key);
              prev = prev.prev;
            }
            errorPath.push(info.path.key);
          }
          errorPath = errorPath.concat(['$args'], path);
          if (error.path !== undefined) {
            errorPath = errorPath.concat(Array.isArray(error.path) ? error.path : [error.path]);
          }

          throw fromGraphQLError(new GraphQLError(
            error.message,
            error.nodes !== undefined ? error.nodes : info.fieldNodes,
            error.source !== undefined ? error.source : source,
            error.positions,
            errorPath,
            error.originalError,
          ), {
            errorClass: <any>(error instanceof ApolloError ? error.constructor : ValidationError),
          });
        }
        throw error;
      }
    }
  }
}
