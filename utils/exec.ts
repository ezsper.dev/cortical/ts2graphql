import { execSync, ExecSyncOptions } from 'child_process';
import * as appRootDir from 'app-root-dir';

export function exec(command: string, options?: ExecSyncOptions): void {
  execSync(command, {
    stdio: 'inherit',
    env: {
      PATH: process.env.PATH,
      ...(options && options.env),
    },
    cwd: appRootDir.get(),
    ...options,
  });
}
