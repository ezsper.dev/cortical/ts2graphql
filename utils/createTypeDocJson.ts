import { spawnSync, SpawnSyncReturns } from 'child_process';
import * as appRootDir from 'app-root-dir';
import { TypeDoc } from './TypeDoc';
import * as path from 'path';
import * as fs from 'fs';

export type Handler = (result: SpawnSyncReturns<Buffer>) => void;

export function createTypeDocJson(inputPath: string, handler?: Handler, stdio: any = 'inherit') {
  const tmpDir = path.resolve(process.cwd(), '.tmp');
  const astJsonPath = path.resolve(tmpDir, `typedoc.json`);
  const result = spawnSync('typedoc', [
    '--excludeExternals',
    '--json', astJsonPath,
    inputPath,
  ], {
    stdio,
    env: {
      PATH: process.env.PATH,
    },
    cwd: appRootDir.get(),
  });
  if (typeof handler === 'function') {
    handler(result);
  }
  if (result.signal != null) {
    throw new Error(`Ended process due to signal ${result.signal}`);
  }
  if (result.error != null) {
    throw result.error;
  }
  if (result.status !== 0) {
    throw new Error(`Could not generate due to typescript error`);
  }
  const typedocJson = require(astJsonPath);
  fs.unlinkSync(astJsonPath);
  try {
    fs.rmdirSync(tmpDir);
  } catch (err) {
  }
  return new TypeDoc(typedocJson);
}
