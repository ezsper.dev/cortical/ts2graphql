/*
 * This is a TypeDoc traversing that help us get informations about the
 * project files
 */

export type TypeDocRefType = {
  type: string,
  types?: TypeDocRefType[],
  name: string,
  id?: number,
  typeArguments?: TypeDocRefType[];
  elementType?: TypeDocRefType;
  declaration?: TypeDocRef,
};

export type TypeDocRefFlags = {
  isExported?: boolean,
  isPrivate?: boolean,
  isStatic?: boolean,
  isProtected?: boolean,
  isOptional?: boolean,
};

export type TypeDocRefSource = {
  fileName: string,
  line: number,
  character: number,
};

export type TypeDocRef = {
  id: number,
  name: string,
  kind: number,
  kindString?: string,
  children?: TypeDocRef[],
  signatures?: TypeDocRef[],
  getSignature?: TypeDocRef,
  setSignature?: TypeDocRef,
  parameters?: TypeDocRef[],
  parent?: TypeDocRef,
  flags: TypeDocRefFlags,
  originalName?: string,
  comment?: {
    shortText?: string,
    tags: {tag: string, text: string}[],
  },
  sources?: TypeDocRefSource[],
  defaultValue?: any,
  type?: TypeDocRefType,
  implementationOf?: TypeDocRefType,
  overwrites?: TypeDocRefType,
  implementedTypes?: TypeDocRefType[],
  extendedTypes?: TypeDocRefType[],
  groups: TypeDocRef[],
};

export class TypeDoc {

  private root: TypeDocRef;
  private index: {[id: number]: TypeDocRef} = {};
  private parent: {[id: number]: TypeDocRef} = {};
  private externalModules: {[path: string]: TypeDocRef} = {};
  private dirs: {[path: string]: TypeDocRef[]} = {};

  constructor(ref: TypeDocRef) {
    const parseDoc = (ref: TypeDocRef): TypeDocRef => {
      this.index[ref.id] = ref;
      if (ref.name === 'Resolver') {
        console.log('**', ref);
      }

      const parseChild = (child: TypeDocRef) => {
        this.parent[child.id] = ref;
        parseDoc(child);
      };

      if (ref.children !== undefined) {
        ref.children.forEach(parseChild);
      }

      if (ref.signatures !== undefined) {
        ref.signatures.forEach(parseChild);
      }

      if (ref.parameters !== undefined) {
        ref.parameters.forEach(parseChild);
      }

      if (typeof ref.type !== 'undefined') {
        if (ref.getSignature) {
          parseChild(ref.getSignature);
        }

        if (ref.setSignature) {
          parseChild(ref.setSignature);
        }

        let types = [ref.type];
        if (ref.type.types !== undefined) {
          types = types.concat(ref.type.types);
        }
        for (const type of types) {
          if (type.declaration) {
            parseChild(type.declaration);
          }
        }
      }

      if (this.isExternalModule(ref) && ref.originalName !== undefined) {
        this.externalModules[ref.originalName] = ref;
        const dir = ref.originalName.replace(/\/([^\/]+)$/, '');
        if (this.dirs[dir] === undefined) {
          this.dirs[dir] = [];
        }
        this.dirs[dir].push(ref);
      }

      return Object.freeze(ref);
    }

    this.root = parseDoc(ref);
  }

  getRoot() {
    return this.root;
  }

  isRoot(ref: TypeDocRef) {
    return ref === this.root;
  }

  hasParent(id: number): boolean {
    return this.parent.hasOwnProperty(`${id}`);
  }

  getParent(id: number) {
    const ref = this.parent[id];
    if (ref === undefined) {
      throw new Error('Could not find parent');
    }
    return ref;
  }

  getParentExternalModule(id: number): TypeDocRef {
    const ref = this.get(id);
    if (this.isExternalModule(ref)) {
      return ref;
    }
    if (this.hasParent(id)) {
      return this.getParentExternalModule(this.getParent(id).id);
    }
    throw new Error('Could not get an external parent');
  }

  has(id: number): boolean {
    return this.index.hasOwnProperty(`${id}`);
  }

  get(id: number) {
    const ref = this.index[id];
    if (ref === undefined) {
      throw new Error('Could not find index');
    }
    return ref;
  }

  hasExternalModule(fileName: string) {
    return this.externalModules.hasOwnProperty(fileName);
  }

  // TODO: rename to findExternalModule
  getExternalModule(fileName: string) {
    const ref = this.externalModules[fileName];
    if (ref === undefined) {
      throw new Error('Could not find external module');
    }
    return ref;
  }

  isExternalModule(ref: TypeDocRef) {
    return (
      ref.kindString === 'External module'
      && ref.flags.isExported
      && Array.isArray(ref.sources)
      && ref.sources.length >= 1
    );
  }

  hasPath(path: string) {
    return this.dirs.hasOwnProperty(path);
  }

  getPath(path: string): TypeDocRef[] {
    const externalModules = this.dirs[path];
    if (externalModules === undefined) {
      throw new Error('Could not find path');
    }
    return externalModules.slice();
  }

}
